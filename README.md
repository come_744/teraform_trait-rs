# Tera forms

Note: There are no official links between this project and Tera.

The goal of this repository is to provide a way to generate HTML forms from Rust
structures, using Tera macros to let the developer customize the rendering of
the forms.

This project is primarily designed for Rust on Waves (see the
[draft](https://codeberg.org/come_744/Rust-on-Waves_draft)) projects: the
default value given to the HTML `action` attribute uses the Rust on Waves URL
generation process, but the developer can easily use an attribute of the form
`#[form_url = "/submit"]` to overwrite this behavior.

This project is in its design phase, choices have to be done before it is ready
for publication.