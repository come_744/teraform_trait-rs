//! # Tera form derive macros
//!
//! The documentation has been moved to the `teraform` crate.
// To avoid a cyclic dev-dependency between `teraform` and `teraform_derive`.

#![allow(missing_docs)]

extern crate proc_macro;
use proc_macro::TokenStream;
use proc_macro2::TokenStream as TokenStream2;
use proc_macro2::TokenTree;
use quote::quote;
use syn::{parse_macro_input, Attribute, Data, DataStruct, DeriveInput, Ident};

// The documentation has been moved to the `teraform` crate
// To avoid a cyclic dev-dependency between `teraform` and `teraform_derive`.
#[proc_macro_derive(
    TeraForm,
    attributes(
        form_url,
        form_for,
        form_submit_text,
        form_macro,
        form_macro_args,
        form_label,
        form_type
    )
)]
pub fn derive_tera_form(item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item);
    derive_tera_form_inner(input)
        .unwrap_or_else(|e| e.into_compile_error())
        .into()
}

fn derive_tera_form_inner(input: DeriveInput) -> Result<TokenStream2, syn::Error> {
    let name = input.ident;
    let data = check_struct(input.data)?;
    let attrs = input.attrs;

    let mut given_form_url = None;
    let mut given_submit_text = None;
    for attr in attrs {
        if attr.path.is_ident("form_url") {
            check_no_conflict(given_form_url, "url", &attr)?;
            given_form_url = Some(parse_name_litstr_attr(attr)?);
        } else if attr.path.is_ident("form_for") {
            check_no_conflict(given_form_url, "url", &attr)?;
            let args = parse_args_attr(attr)?;
            given_form_url = Some(format!(r#"{{{{ url_for({}) }}}}"#, args));
        } else if attr.path.is_ident("form_submit_text") {
            check_no_conflict(given_submit_text, "submit text", &attr)?;
            given_submit_text = Some(parse_name_litstr_attr(attr)?);
        } else if attr.path.is_ident("form_macro")
            || attr.path.is_ident("form_macro_args")
            || attr.path.is_ident("form_type")
            || attr.path.is_ident("form_label")
        {
            return Err(unexpected_attribute_err(attr));
        }
    }

    let form_url = match given_form_url {
        Some(url) => url,
        None => default_url_from_item_name(&name)?,
    };

    let mut form = format!(
        //r#"{{% import "form_macros.html" as form %}}<form action="{}" method="POST">"#,
        r#"<form action="{}" method="POST">"#,
        form_url
    );
    for field in data.fields {
        if let Some(field_name) = field.ident {
            let mut additional = String::new();

            let mut mac = None;
            let mut form_type = None;
            let mut label = None;
            for attr in field.attrs {
                if attr.path.is_ident("form_macro") {
                    check_no_conflict(mac, "macro", &attr)?;
                    mac = Some(parse_name_litstr_attr(attr)?);
                } else if attr.path.is_ident("form_macro_args") {
                    let args = parse_args_attr(attr)?;
                    additional.push_str(", ");
                    additional.push_str(&args);
                } else if attr.path.is_ident("form_type") {
                    check_no_conflict(form_type, "type", &attr)?;
                    form_type = Some(parse_name_litstr_attr(attr)?);
                } else if attr.path.is_ident("form_label") {
                    check_no_conflict(label, "label", &attr)?;
                    label = Some(parse_name_litstr_attr(attr)?);
                } else if attr.path.is_ident("form_url")
                    || attr.path.is_ident("form_for")
                    || attr.path.is_ident("form_submit_text")
                {
                    return Err(unexpected_attribute_err(attr));
                }
            }

            if form_type.is_none() {
                let field_name = field_name.to_string();
                match field.vis {
                    syn::Visibility::Public(_) => match field_name.as_str() {
                        "email" | "password" => form_type = Some(field_name),
                        _ => {}
                    },
                    _ => form_type = Some(String::from("password")),
                }
            }

            if let Some(form_type) = form_type {
                if !form_type.is_empty() {
                    additional.push_str(&format!(r#", type="{}""#, form_type))
                }
            }

            let input = format!(
                r#"{{{{ form::{mac}(id="{id}", label="{label}"{add}) }}}}"#,
                add = additional,
                label = label.unwrap_or_else(|| default_label_for(&field_name)),
                id = field_name,
                mac = mac.as_deref().unwrap_or("input"),
            );

            form.push_str(&input);
        }
    }
    if let Some(submit_text) = given_submit_text {
        form.push_str(&format!(
            r#"{{{{ form::submit(text="{}") }}}}</form>"#,
            submit_text
        ));
    } else {
        form.push_str("{{ form::submit() }}</form>");
    }

    Ok(quote! {
        impl TeraForm for #name {
            const TERA_FORM: &'static str = #form;
        }
    })
}

fn parse_name_litstr_attr(attr: syn::Attribute) -> Result<String, syn::Error> {
    match attr.parse_meta()? {
        syn::Meta::NameValue(mnv) => match mnv.lit {
            syn::Lit::Str(lit_str) => Ok(lit_str.value()),
            lit => Err(syn::Error::new(lit.span(), "expected a string literal")),
        },
        _ => Err(syn::Error::new(
            attr.bracket_token.span,
            format!(
                "expected `=` after `{}`",
                attr.path.segments.last().unwrap().ident
            ),
        )),
    }
}

fn parse_args_attr(attr: syn::Attribute) -> Result<String, syn::Error> {
    let mut iter = attr.tokens.into_iter();
    let args = if let Some(TokenTree::Group(g)) = iter.next() {
        g.stream()
    } else {
        return Err(syn::Error::new(
            attr.bracket_token.span,
            &format!(
                "expected `{}(..)`",
                attr.path.segments.last().unwrap().ident
            ),
        ));
    };
    if let Some(tt) = iter.next() {
        return Err(syn::Error::new(
            tt.span(),
            "Unexpected additional token tree",
        ));
    }
    Ok(args.to_string())
}

fn unexpected_attribute_err(attr: syn::Attribute) -> syn::Error {
    syn::Error::new(
        attr.bracket_token.span,
        format!(
            "unexpected attribute `{}` here",
            attr.path.segments.last().unwrap().ident
        ),
    )
}

fn check_struct(data: Data) -> Result<DataStruct, syn::Error> {
    match data {
        syn::Data::Struct(data) => Ok(data),
        syn::Data::Enum(data) => Err(syn::Error::new(
            data.enum_token.span,
            "TeraForm derive macro can only be applied to a struct",
        )),
        syn::Data::Union(data) => Err(syn::Error::new(
            data.union_token.span,
            "TeraForm derive macro can only be applied to a struct",
        )),
    }
}

fn check_no_conflict(data: Option<String>, what: &str, attr: &Attribute) -> Result<(), syn::Error> {
    match data {
        Some(_) => Err(syn::Error::new(
            attr.bracket_token.span,
            format!("conflicting {} definition", what),
        )),
        None => Ok(()),
    }
}

fn default_label_for(ident: &Ident) -> String {
    let id = ident.to_string();
    let mut label = String::with_capacity(id.len() + 1);
    let mut iter = id.chars();
    if let Some(first) = iter.next() {
        label.extend(first.to_uppercase());
    }
    label.extend(iter.map(|c| if c == '_' { ' ' } else { c }));
    label.push(':');
    label
}

fn default_url_from_item_name(name: &Ident) -> Result<String, syn::Error> {
    Ok(format!(
        r#"{{{{ url_for({}="", action="{}") }}}}"#,
        default_resource_from_item_name(name),
        default_action_from_item_name(name).ok_or_else(|| syn::Error::new(
            name.span(),
            "impossible to guess a url from this name, please define a url manually"
        ))?
    ))
}

fn default_resource_from_item_name(ident: &Ident) -> String {
    let owned_name = ident.to_string();
    let name = owned_name.as_str();
    // TODO first strip optional suffix 'Form', then strip from first letter to first next capital,
    // then if not empty take the lower case else take before stripping prefix
    let name_no_pre = name.strip_prefix("New").unwrap_or(name);
    let name_no_suf = name_no_pre.strip_suffix("Form").unwrap_or(name_no_pre);
    name_no_suf.to_lowercase()
}

fn default_action_from_item_name(ident: &Ident) -> Option<&'static str> {
    // TODO keep from first letter to first next capital
    let owned_name = ident.to_string();
    let name = owned_name.as_str();
    if name.starts_with("New") || name.starts_with("Create") {
        Some("create")
    } else if name.starts_with("Edit") || name.starts_with("Update") {
        Some("update")
    } else {
        None
    }
}
