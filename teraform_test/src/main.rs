use tera::{Context, Tera};
use teraform_trait::TeraForm;

#[derive(TeraForm)]
#[form_url = "/signup"]
#[form_submit_text = "Sign Up"]
pub struct NewUserForm {
    pub username: String,
    pub email: String,
    #[allow(dead_code)]
    #[form_label = "New password:"]
    password: String,
    pub some_text: String,
}

fn main() {
    let mut tera = Tera::new("templates/**/*.html").unwrap();
    tera.add_raw_template("user/_new_form.html", NewUserForm::TERA_FORM)
        .unwrap();

    println!("Template: {}", NewUserForm::TERA_FORM);
    /*
    let form = tera
        .render_str(NewUserForm::TERA_FORM, &Context::new())
        .unwrap();
    println!("Form: {}", form);
    */

    let mut ctx = Context::new();
    ctx.insert("_base__title", "My Page");
    //ctx.insert("form", &form);
    ctx.insert("form", "");

    let page = tera.render("user/signup.html", &ctx).unwrap();
    println!("Page: {page}");
}
