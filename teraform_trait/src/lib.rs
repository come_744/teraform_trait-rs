//! # Tera forms
//!
//! Tera forms is a Rust derive macro to generate HTML forms from Rust `struct`s.
//!
//! The goal of generating forms from `struct`s is to avoid bugs when the developer forgets to keep
//! in sync the `struct` and the form. However, it obviously does not replace server-side form
//! verification.
//!
//! TODO point to some famous form verification crates.
//!
//! The generation is done in two steps:
//!
//! 1. At compile time, a Tera template is generated. This template uses user-defined Tera macros.
//! 2. At run time, Tera expands the macros into actual HTML.
//!
//! TODO does Tera expand macros with const arguments once for all on startup? It could help
//! determine the runtime cost of this way to generate forms.
//!
//! See [`TeraForm`] for an example.

#![warn(missing_docs)]

#[allow(unused_imports)]
#[macro_use]
extern crate teraform_derive;

/// Derive macro to generate HTML forms from Rust `struct`s.
///
/// # Examples
///
/// ```no_run
/// use tera::{Context, Tera};
/// use teraform_trait::TeraForm;
///
/// #[derive(TeraForm)]
/// #[form_url = "/signup"]
/// #[form_submit_text = "Sign Up"]
/// pub struct NewUserForm {
///     pub username: String,
///     pub email: String,
///     #[form_label = "New password:"]
///     password: String,
/// }
///
/// let mut tera = Tera::new("templates/**/*.html")?;
/// tera.add_raw_template("user/_new_form.html", NewUserForm::TERA_FORM)?;
///
/// # Ok::<(), tera::Error>(())
/// ```
///
/// The template `NewUserForm::TERA_FORM` is generated with the following content (formatted here
/// for readability):
///
/// ```html
/// <form action="/signup" method="POST">
///   {{ form::input(id="username", label="Username:") }}
///   {{ form::input(id="email", label="Email:", type="email") }}
///   {{ form::input(id="password", label="New password:", type="password") }}
///   {{ form::submit(text="Sign Up") }}
/// </form>
/// ```
///
/// Which could then render like following, depending on your `form::input()` Tera macros.
///
/// ```html
/// <form action="/signup" method="POST">
///   <div>
///     <label for="username">Username:</label>
///     <input type="text" id="username" name="username">
///   </div>
///     <label for="email">Email:</label>
///     <input type="email" id="email" name="email">
///   </div>
///   <div>
///     <label for="password">New password:</label>
///     <input type="password" id="password" name="password">
///   </div>
///   <input type="submit" value="Sign Up">
/// </form>
/// ```
///
/// # Attributes
///
/// | [Item attribute](#item-attributes)      | Short description                           |
/// | :---                                    | :---                                        |
/// | [`form_url`](#form_url)                 | define the URL                              |
/// | [`form_for`](#form_for)                 | define the URL using the RoW URL gen system |
/// | [`form_submit_text`](#form_submit_text) | define the submit button text               |
///
/// | [Field attribute](#field-attributes)  | Short description                              |
/// | :---                                  | :---                                           |
/// | [`form_macro`](#form_macro)           | define the Tera macro to use                   |
/// | [`form_macro_args`](#form_macro_args) | add extra arguments for the Tera macro         |
/// | [`form_label`](#form_label)           | define the label for the form field            |
/// | [`form_type`](#form_type)             | define `type=` attribute for (`<input>`) field |
///
/// ## Item attributes
///
/// ### `form_url`
///
/// Provide an URL for the `action` attribute of the `form` HTML tag. Must be provided exactly
/// once. It is possible to omit this attribute if both the two following conditions are met:
///
/// 1. The Rust on Waves URL generation system is available in the current crate.
/// 1. The `struct` name clearly describes a resource or the [`form_for`](#form_for) attribute is
///    used.
///
/// ```
/// # use teraform_trait::TeraForm;
/// #[derive(TeraForm)]
/// #[form_url = "/submit"]
/// struct MyForm;
/// # assert_eq!(
/// #     MyForm::TERA_FORM,
/// #     r#"<form action="/submit" method="POST">{{ form::submit() }}</form>"#
/// # );
/// ```
///
/// If omitted, the default value of the URL is as described [below](#form_for).
///
/// ### `form_for`
///
/// Use `url_for()` Tera function from the Rust on Waves URL generation system to set the `action`
/// attribute of the `form` HTML tag. The contents provided between parentheses is directly given
/// to the Tera function, without interpretation, but they are formatted by `rustc` with spaces
/// around the `=` characters. It should not have any effect on Tera's behavior.
///
/// This attribute can be used only if:
///
/// 1. The Rust on Waves URL generation system is available in the current crate.
/// 1. The `form_url` attribute is not used on the current item.
///
/// ```
/// # use teraform_trait::TeraForm;
/// #[derive(TeraForm)]
/// #[form_for(user="", action="create")]
/// struct MyForm;
/// # assert_eq!(
/// #     MyForm::TERA_FORM,
/// #     r#"<form action="{{ url_for(user = "", action = "create") }}" method="POST">{{ form::submit() }}</form>"#
/// # );
/// ```
///
/// TODO improve the rules to build the default URL, then document it
///
/// ### `form_submit_text`
///
/// Choose the text displayed on the submit button of the form.
///
/// ```
/// # use teraform_trait::TeraForm;
/// #[derive(TeraForm)]
/// #[form_url = "/submit"]
/// #[form_submit_text = "Go!"]
/// struct MyForm;
/// # assert_eq!(
/// #     MyForm::TERA_FORM,
/// #     r#"<form action="/submit" method="POST">{{ form::submit(text="Go!") }}</form>"#
/// # );
/// ```
///
/// As an effect, an optional `text=` argument will be added to the `form::submit` Tera macro, with
/// the value you provided:
///
/// ```html
/// {{ form::submit(text="Go!") }}
/// ```
///
/// Which can change the HTML, depending on your definition of the `form::submit` Tera macro, for
/// instance:
///
/// ```html
/// <input type="submit" value="Go!">
/// ```
///
/// If this attribute is not given, no argument is given to the `form::submit()` Tera macro, so the
/// default text for the submit button is defined by your implementation of the macro:
///
/// ```html
/// {{ form::submit() }}
/// ```
///
/// ## Field attributes
///
/// ### `form_macro`
///
/// Choose the name of the Tera macro to use, in the `form::` namespace.
///
/// Optional. Defaults to `input`.
///
/// ```
/// # use teraform_trait::TeraForm;
/// #[derive(TeraForm)]
/// #[form_url = "/submit"]
/// struct MyForm {
///     #[form_macro = "textarea"]
///     pub some_text: String,
/// }
/// # assert_eq!(
/// #     MyForm::TERA_FORM,
/// #     r#"<form action="/submit" method="POST">{{ form::textarea(id="some_text", label="Some text:") }}{{ form::submit() }}</form>"#
/// # );
/// ```
///
/// produces:
///
/// ```html
/// {{ form::textarea(id="some_text", label="Some text:") }}
/// ```
///
/// instead of:
///
/// ```html
/// {{ form::input(id="some_text", label="Some text:") }}
/// ```
///
/// ### `form_macro_args`
///
/// Append extra arguments for the Tera macro. The arguments are not parsed by the macro, but they
/// are formatted by `rustc` with spaces around the `=` characters. It should not have any effect
/// on Tera's behavior.
///
/// ```
/// # use teraform_trait::TeraForm;
/// #[derive(TeraForm)]
/// #[form_url = "/submit"]
/// struct MyForm {
///     #[form_macro_args(hidden="true")]
///     pub some_text: String,
/// }
/// # assert_eq!(
/// #     MyForm::TERA_FORM,
/// #     r#"<form action="/submit" method="POST">{{ form::input(id="some_text", label="Some text:", hidden = "true") }}{{ form::submit() }}</form>"#
/// # );
/// ```
///
/// produces:
///
/// ```html
/// {{ form::input(id="some_text", label="Some text:", hidden = "true") }}
/// ```
///
/// instead of:
///
/// ```html
/// {{ form::input(id="some_text", label="Some text:") }}
/// ```
///
/// Note: the Tera macro may ignore the extra arguments. It is up to you to define your Tera
/// macros.
///
/// ### `form_label`
///
/// Define the text to give to the label.
///
/// Optional. Defaults to the name of the field after the following modifications:
///
/// - the first character is put uppercase
/// - underscores are replaced by spaces.
/// - suffixed with `:`
///
/// ```
/// # use teraform_trait::TeraForm;
/// #[derive(TeraForm)]
/// #[form_url = "/submit"]
/// struct MyForm {
///     #[form_label = "My text"]
///     pub some_text: String,
/// }
/// # assert_eq!(
/// #     MyForm::TERA_FORM,
/// #     r#"<form action="/submit" method="POST">{{ form::input(id="some_text", label="My text") }}{{ form::submit() }}</form>"#
/// # );
/// ```
///
/// produces:
///
/// ```html
/// {{ form::input(id="some_text", label="My text") }}
/// ```
///
/// instead of:
///
/// ```html
/// {{ form::input(id="some_text", label="Some text:") }}
/// ```
///
/// It is intended to render something like:
///
/// ```html
/// <div>
///   <label for="some_text">My text</label>
///   <input type="text" id="some_text" name="some_text">
/// </div>
/// ```
///
/// instead of:
///
/// ```html
/// <div>
///   <label for="some_text">Some text:</label>
///   <input type="text" id="some_text" name="some_text">
/// </div>
/// ```
///
/// ### `form_type`
///
/// Define the `type=` argument for the macro, which is should be given to the `type=` attribute of
/// the (`<input>`) HTML tag.
///
/// Optional. Defaults to:
///
/// - If the item is `pub`
///     - If the item name is `email`, then `email`
///     - If the item name is `password`, then `password`
///     - Else, nothing
/// - Else, `password`
///
/// ```
/// # use teraform_trait::TeraForm;
/// #[derive(TeraForm)]
/// #[form_url = "/submit"]
/// struct MyForm {
///     #[form_type = "password"]
///     pub some_text: String,
/// }
/// # assert_eq!(
/// #     MyForm::TERA_FORM,
/// #     r#"<form action="/submit" method="POST">{{ form::input(id="some_text", label="Some text:", type="password") }}{{ form::submit() }}</form>"#
/// # );
/// ```
///
/// produces:
///
/// ```html
/// {{ form::input(id="some_text", label="Some text:", type="password") }}
/// ```
///
/// instead of:
///
/// ```html
/// {{ form::input(id="some_text", label="Some text:") }}
/// ```
///
/// It is intended to render something like:
///
/// ```html
/// <div>
///   <label for="some_text">Some text:</label>
///   <input type="password" id="some_text" name="some_text">
/// </div>
/// ```
///
/// instead of:
///
/// ```html
/// <div>
///   <label for="some_text">Some text:</label>
///   <input type="text" id="some_text" name="some_text">
/// </div>
/// ```
pub use teraform_derive::TeraForm;

/// This trait is meant to be implemented with [`derive(Teraform)`](teraform_derive::TeraForm).
///
/// # Examples
///
/// This is a short example. For more explanations about customizing the generated template, see
/// [`teraform_derive::TeraForm`]. For more explanation about customizing the HTML output with Tera
/// macros, see the [Tera documentation](https://tera.netlify.app/docs/#macros).
///
/// ```no_run
/// use tera::{Context, Tera};
/// use teraform_trait::TeraForm;
///
/// #[derive(TeraForm)]
/// #[form_url = "/signup"]
/// pub struct NewUserForm {
///     pub username: String,
///     pub email: String,
///     password: String,
/// }
///
/// let mut tera = Tera::new("templates/**/*.html")?;
/// tera.add_raw_template("user/_new_form.html", NewUserForm::TERA_FORM)?;
///
/// # Ok::<(), tera::Error>(())
/// ```
///
/// The template `NewUserForm::TERA_FORM` has the following content (formatted here for
/// readability):
///
/// ```html
/// <form action="/signup" method="POST">
///   {{ form::input(id="username", label="Username:") }}
///   {{ form::input(id="email", label="Email:", type="email") }}
///   {{ form::input(id="password", label="Password:", type="password") }}
///   {{ form::submit() }}
/// </form>
/// ```
///
/// Which could then render like following, depending on your Tera macros:
///
/// ```html
/// <form action="/signup" method="POST">
///   <div>
///     <label for="username">Username:</label>
///     <input type="text" id="username" name="username">
///   </div>
///     <label for="email">Email:</label>
///     <input type="email" id="email" name="email">
///   </div>
///   <div>
///     <label for="password">Password:</label>
///     <input type="password" id="password" name="password">
///   </div>
///   <input type="submit" value="Submit">
/// </form>
/// ```
pub trait TeraForm {
    /// The statically generated Tera template to build the form.
    const TERA_FORM: &'static str;
}
